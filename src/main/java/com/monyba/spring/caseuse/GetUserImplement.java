package com.monyba.spring.caseuse;

import com.monyba.spring.entity.Users;
import com.monyba.spring.service.UserService;

import java.util.List;

public class GetUserImplement implements GetUser{
  private UserService userService;

  public GetUserImplement(UserService userService) {
    this.userService = userService;
  }

  @Override
  public List<Users> getAll() {
    return userService.getAllUsers();
  }
}
