package com.monyba.spring.caseuse;

import com.monyba.spring.entity.Users;
import com.monyba.spring.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class CreateUser {

  private UserService userService;

  public CreateUser(UserService userService) {
    this.userService = userService;
  }


  public Users save(Users newUser) {
    return userService.save(newUser);
  }
}
