package com.monyba.spring.caseuse;

import com.monyba.spring.entity.Users;

import java.util.List;

public interface GetUser {
  List<Users> getAll();
}
