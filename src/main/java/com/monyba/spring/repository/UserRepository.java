package com.monyba.spring.repository;

import com.monyba.spring.dto.UserDto;
import com.monyba.spring.entity.Users;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
  @Query("select u from Users u where u.email=?1")
  Optional<Users> findByUserEmail(String email);

  @Query("select u from Users u where u.name like ?1%")
  List<Users> findAndSort(String name, Sort sort);

  List<Users> findByName(String name);

  Optional<Users> findByEmailAndName(String email, String name);

  List<Users> findByNameLike(String name);

  List<Users> findByNameOrEmail(String name, String email);

  List<Users> findByBirthDateBetween(LocalDate startDate, LocalDate endDate);

  List<Users> findByNameLikeOrderByIdDesc(String name);

  List<Users> findByNameContainsOrderByIdDesc(String name);

  @Query("select new com.monyba.spring.dto.UserDto(u.id, u.name, u.birthDate) " +
          "from Users u " +
          "where u.birthDate=:parametroFecha " +
          "and u.email=:parametroEmail ")
  Optional<UserDto> getAllByBirthDateAndEmail(@Param("parametroFecha") LocalDate date,
                                              @Param("parametroEmail") String email);

}
