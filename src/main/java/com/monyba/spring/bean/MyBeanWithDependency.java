package com.monyba.spring.bean;

public interface MyBeanWithDependency {
  void printWithDependency();
}
