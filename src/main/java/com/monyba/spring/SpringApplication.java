package com.monyba.spring;

import com.monyba.spring.bean.MyBean;
import com.monyba.spring.bean.MyBeanWithDependency;
import com.monyba.spring.bean.MyBeanWithProperties;
import com.monyba.spring.component.ComponentDependency;
import com.monyba.spring.entity.Users;
import com.monyba.spring.pojo.UserPojo;
import com.monyba.spring.repository.UserRepository;
import com.monyba.spring.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class SpringApplication implements CommandLineRunner {

	Log LOGGER = LogFactory.getLog(SpringApplication.class);

	private ComponentDependency componentDependency;
	private MyBean myBean;
	private MyBeanWithDependency myBeanWithDependency;
	private MyBeanWithProperties myBeanWithProperties;
	private UserPojo userPojo;
	private UserRepository userRepository;

	private UserService userService;


	public SpringApplication(@Qualifier("componentTwoImplement") ComponentDependency componentDependency,
													 @Qualifier("beanOperation1") MyBean myBean,
													 MyBeanWithDependency myBeanWithDependency,
													 MyBeanWithProperties myBeanWithProperties,
													 UserPojo userPojo,
													 UserRepository userRepository,
													 UserService userService) {
		this.componentDependency = componentDependency;
		this.myBean = myBean;
		this.myBeanWithDependency = myBeanWithDependency;
		this.myBeanWithProperties = myBeanWithProperties;
		this.userPojo = userPojo;
		this.userRepository = userRepository;
		this.userService = userService;
	}

	public static void main(String[] args) {
		org.springframework.boot.SpringApplication.run(SpringApplication.class, args);
	}

	private void dependencias(){
		componentDependency.saludar();
		myBean.print();
		myBeanWithDependency.printWithDependency();
		System.out.println(myBeanWithProperties.function());
		System.out.println(userPojo.toString());
		LOGGER.error("Esto es un error del aplicativo");
	}

	private void persistencia(){
		Users user1 = new Users("John", "john@domain.com", LocalDate.of(2021, 3, 13));
		Users user2 = new Users("Marco", "marco@domain.com", LocalDate.of(2021, 12, 8));
		Users user3 = new Users("Daniela", "daniela@domain.com", LocalDate.of(2021, 9, 8));
		Users user4 = new Users("Marisol", "marisol@domain.com", LocalDate.of(2021, 6, 18));
		Users user5 = new Users("Karen", "karen@domain.com", LocalDate.of(2021, 1, 1));
		Users user6 = new Users("Carlos", "carlos@domain.com", LocalDate.of(2021, 7, 7));
		Users user7 = new Users("Enrique", "enrique@domain.com", LocalDate.of(2021, 11, 12));
		Users user8 = new Users("Luis", "luis@domain.com", LocalDate.of(2021, 2, 27));
		Users user9 = new Users("Paola", "paola@domain.com", LocalDate.of(2021, 4, 10));
		List<Users> users = Arrays.asList(user1, user2, user3, user4, user5, user6, user7, user8, user9);
		users.stream().forEach(userRepository::save);
	}

	private void getInformationJpqlFromUser(){
		userRepository.findByUserEmail("karen2@domain.com").ifPresentOrElse(users -> LOGGER.info(users.toString()),()-> LOGGER.warn("No se encontro el usuario"));

		userRepository.findAndSort("Ma", Sort.by("id").ascending()).stream().forEach(users -> LOGGER.info(users));

		userRepository.findByName("Enrique").stream().forEach(users -> LOGGER.info(users));

		userRepository.findByEmailAndName("enrique@domain.com", "Enrique").ifPresentOrElse(users -> LOGGER.info(users.toString()),()-> LOGGER.warn("No se encontro el usuario"));

		userRepository.findByNameLike("%Ma%").stream().forEach(users -> LOGGER.info(users));

		userRepository.findByNameOrEmail( "Enrique", "carlos@domain.com").stream().forEach(users -> LOGGER.info(users.toString()));

		userRepository.findByBirthDateBetween(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 4, 30)).stream().forEach(users -> LOGGER.info(users.toString()));

		userRepository.findByNameLikeOrderByIdDesc("%Ma%").stream().forEach(users -> LOGGER.info(users.toString()));

		userRepository.findByNameLikeOrderByIdDesc("Luis").stream().forEach(users -> LOGGER.info(users.toString()));

		userRepository.getAllByBirthDateAndEmail(LocalDate.of(2021, 12, 8), "marco@domain.com").ifPresentOrElse(users -> LOGGER.info(users.toString()),()-> LOGGER.warn("No se encontro el usuario"));
	}

	private void saveWithErrorTransactional(){
		Users test1 = new Users("TestTransactional1", "TestTransactional1@domain.com", LocalDate.now());
		Users test2 = new Users("TestTransactional2", "TestTransactional2@domain.com", LocalDate.now());
		Users test3 = new Users("TestTransactional3", "TestTransactional3@domain.com", LocalDate.now());
		Users test4 = new Users("TestTransactional4", "TestTransactional4@domain.com", LocalDate.now());

		List<Users> users = Arrays.asList(test1, test2, test3, test4);
		try {
			userService.saveTransactional(users);
		}catch (Exception e){
			LOGGER.error("Se produjo un error al insertar los Usuarios: " + e.getMessage());
		}
		userService.getAllUsers().stream().forEach(user -> LOGGER.info(user.toString()));
	}

	@Override
	public void run(String... args) throws Exception {
		//dependencias();
		//persistencia();
		//getInformationJpqlFromUser();
		saveWithErrorTransactional();
	}
}
