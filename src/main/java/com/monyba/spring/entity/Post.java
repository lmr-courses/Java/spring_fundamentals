package com.monyba.spring.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
@Table(name = "post")
public class Post {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id_post", nullable = false, unique = true)
  private Long id;

  @Column(name="description", length = 255)
  private String description;

  // Muchos Post pertenecen a un Usuario
  @ManyToOne
  @JsonBackReference
  private Users users;

  public Post() {
  }

  public Post(Long id, String description, Users users) {
    this.id = id;
    this.description = description;
    this.users = users;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Users getUsers() {
    return users;
  }

  public void setUsers(Users users) {
    this.users = users;
  }

  @Override
  public String toString() {
    return "Post{" +
            "id=" + id +
            ", description='" + description + '\'' +
            ", user=" + users +
            '}';
  }
}
