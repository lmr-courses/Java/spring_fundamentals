package com.monyba.spring.configuration;

import com.monyba.spring.bean.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfigurationBean {
  @Bean
  public MyBean beanOperation1(){
    return new MyBeanImplement();
  }

  @Bean
  public MyBean beanOperation2(){
    return new MyBeanTwoImplement();
  }

  @Bean
  public MyOperation beanOperation3(){
    return new MyOperationImplement();
  }

  @Bean
  public MyBeanWithDependency beanOperation4(MyOperation myOperation){
    return new MyBeanWithDependencyImplement(myOperation);
  }

}
