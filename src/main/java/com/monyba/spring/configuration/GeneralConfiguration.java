package com.monyba.spring.configuration;

import com.monyba.spring.bean.MyBeanWithProperties;
import com.monyba.spring.bean.MyBeanWithPropertiesImplement;
import com.monyba.spring.pojo.UserPojo;
import jakarta.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:connection.properties")
@EnableConfigurationProperties(UserPojo.class)
public class GeneralConfiguration {
  @Value("${value.name}")
  private String name;

  @Value("${value.apellido}")
  private String apellido;

  @Value("${value.random}")
  private String random;

  @Value("${jdbc.url}")
  @NotBlank
  private String jdbcUrl;

  @Value("${driver}")
  @NotBlank
  private String driver;

  @Value("${username}")
  @NotBlank
  private String username;

  @Value("${password}")
  @NotBlank
  private String password;

  @Bean
  public MyBeanWithProperties function(){
    return new MyBeanWithPropertiesImplement(name, apellido);
  }

  @Bean
  public DataSource dataSource(){
    DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
    dataSourceBuilder.driverClassName(driver);
    dataSourceBuilder.url(jdbcUrl);
    dataSourceBuilder.username(username);
    dataSourceBuilder.password(password);
    return dataSourceBuilder.build();
  }

}
