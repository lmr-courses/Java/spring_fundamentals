package com.monyba.spring.configuration;

import com.monyba.spring.caseuse.GetUser;
import com.monyba.spring.caseuse.GetUserImplement;
import com.monyba.spring.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CaseUseConfiguration {

  @Bean
  GetUser getUser(UserService userService){
    return new GetUserImplement(userService);
  }

}
