package com.monyba.spring.controller;

import com.monyba.spring.caseuse.CreateUser;
import com.monyba.spring.caseuse.DeleteUser;
import com.monyba.spring.caseuse.GetUser;
import com.monyba.spring.caseuse.UpdateUser;
import com.monyba.spring.entity.Users;
import com.monyba.spring.repository.UserRepositorypage;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@RestController
@RequestMapping(value = "/api/users")
public class UserRestController {
  private GetUser getUser;
  private CreateUser createUser;
  private DeleteUser deleteUser;
  private UpdateUser updateUser;
  private UserRepositorypage userRepositorypage;

  public UserRestController(GetUser getUser, CreateUser createUser, DeleteUser deleteUser, UpdateUser updateUser, UserRepositorypage userRepositorypage) {
    this.getUser = getUser;
    this.createUser = createUser;
    this.deleteUser = deleteUser;
    this.updateUser = updateUser;
    this.userRepositorypage = userRepositorypage;
  }

  @GetMapping("/")
  List<Users> get(){
    return getUser.getAll();
  }

  @PostMapping("/")
  ResponseEntity<Users> newUser(@RequestBody Users newUser){
    return new ResponseEntity<>(createUser.save(newUser), HttpStatus.CREATED);
  }

  @DeleteMapping("/{id}")
  ResponseEntity deleteUser(@PathVariable Long id){
    deleteUser.remove(id);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @PutMapping("/{id}")
  ResponseEntity<Users> updateUser(@RequestBody Users newUser, @PathVariable Long id){
    return new ResponseEntity<>(updateUser.update(newUser, id), HttpStatus.OK);
  }

  @GetMapping("/pageable")
  List<Users> getUserPageable(@RequestParam int page, @RequestParam int size){
    return userRepositorypage.findAll(PageRequest.of(page, size)).getContent();
  }

}
