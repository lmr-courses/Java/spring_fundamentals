package com.monyba.spring.service;

import com.monyba.spring.entity.Users;
import com.monyba.spring.repository.UserRepository;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
  private final Log LOG = LogFactory.getLog(UserService.class);
  private UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Transactional
  public void saveTransactional(List<Users> users){
    users.stream().peek(user -> LOG.info("Usuario insertado: " + user.toString())).forEach(userRepository::save);
  }

  public List<Users> getAllUsers(){
    return userRepository.findAll();
  }

  public Users save(Users newUser) {
    return userRepository.save(newUser);
  }

  public void delete(Long id) {
    userRepository.deleteById(id);
  }

  public Users update(Users newUser, Long id) {
    return userRepository.findById(id).map(user -> {
      user.setBirthDate(newUser.getBirthDate());
      user.setEmail(newUser.getEmail());
      user.setName(newUser.getName());
      return userRepository.save(user);
    }).get();
  }
}
